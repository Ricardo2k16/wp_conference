<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'raspbimysql');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ';rU;=Mpl)]BA#,X&Mc|TM#!%*6u`2k?%b/9l*w8y6XD7[t$ztXEwv4TW+A/a!>,W');
define('SECURE_AUTH_KEY',  'f{M4IPtdSWhOHT(~a=sN--)hchXSfYyBIO__<(-v8I&@,y>oAyWg?4Cp0~is3Ol0');
define('LOGGED_IN_KEY',    '`ZVrij<%tZHS[3;fl`9<i?Y#e;Y=QqW]! <:,?(z^uWHSBp-}8:kR,EBFXmS!@Gu');
define('NONCE_KEY',        ')#mdilw@Q4OS[-lLryCxDp)=V:SwOj26]/[*Bn<{]qtW,$V>,vr3Z|Jj;)z~N^T&');
define('AUTH_SALT',        'yTYJR~~o@Kul;}u;l%Enlah-IV}7clAm)J}J/|V5%w${6 P#/4VdZ/))[}&9iOKE');
define('SECURE_AUTH_SALT', 'c,$c]7PMR3A)RpvT,JC}X+-f28B-`@RnK l@-lxh3a+2_8-S7K?/Kk&xy-z@: @Y');
define('LOGGED_IN_SALT',   'yW,LqZN?7L!(3oQW;+nPyE1-1v.r@_-Qk_.gW1qUO]Uf9]O/WdNwi0alN%-Gh,MR');
define('NONCE_SALT',       'c.8KNtQIw{LwYJ.Z1,.`C2lSc1?2v4#y.OATc0)YP2G#eNGEi#+]0{)BHA,7FiA%');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
